import ferma as frm
import Lonzheron as lonz
import Panel as pan
import Stringer_calc as strng
from docxtpl import DocxTemplate, InlineImage
from PIL import Image
from Plot_Ferma import plot_all_frm
from Plot_Longeron import plot_all_lonz
from Plot_Panel import plot_all_pan
from Plot_Ugolok import plot_all_strng

# создание словаря для титульного листа документа
context_top = {
    "name": "Фамилия Имя Отчество",
    "group": "№ группы",
    "var": "№ варианта",
}

# создание словаря для записи констант
constants = {
    "D": strng.D * 1000,
    "L": strng.L * 1000,
    "N": strng.N / 1000,
    "M": strng.M / 1000,
    "f": strng.f,
    "E": strng.E / 10**6,
    "SIGMA_T": strng.SIGMA_T / 10**6,
    "SIGMA_V": strng.SIGMA_V / 10**6,
    "RO": strng.RO,
    "DELTHA": strng.DELTHA * 1000,
}

# создание словаря для записи переменных стрингерного отсека
variables_stringer = {
    "Ne": round(strng.Ne / 1000, 2),
    "Nr": round(strng.Nr / 1000, 2),
    "sigma_r": strng.sigma_r / 10**6,
    "n_pr": strng.n_pr,
    "n": strng.n,
    "b": round(strng.b * 1000, 1),
    "Nc": round(strng.Nc / 1000, 2),
    "F": round(strng.F * 10**6, 1),
    "sigma_d": round(strng.sigma_d / 10**6, 1),
    "sigma_m_kr_st": round(strng.sigma_m_kr / 10**6, 1),
    "y": strng.y * 1000,
    "J": round(strng.J * 10**12, 1),
    "lyambda": round(strng.lyambda, 1),
    "sigma_kr_E": round(strng.sigma_kr_E / 10**6, 1),
    "KZ_ust": strng.KZ_ust,
    "MassSum": strng.MassSum,
}

# создание словаря для записи переменных лонжеронного отсека
variables_lonzheron = {
    "Ne_lon": round(lonz.Ne_lon / 1000, 2),
    "Nr_lon": round(lonz.Nr_lon / 1000, 2),
    "sigma_lon": round(lonz.sigma_lon / 10**6, 1),  # ceil
    "n_lon": lonz.n_lon,
    "b_pr": round(lonz.b_pr_lon, 3),
    "Pr_obsh": round(lonz.Pr_obsh, 3),
    "s_pr": round(lonz.s_pr * 10**6, 1),
    "N_lon": round(lonz.N_lon / 1000, 1),
    "F_lon": round(lonz.F_lon * 10**4, 1),
    "H_lon": round(lonz.H_lon, 3),
    "sigma_kr_mestn": round(lonz.sigma_kr_mestn / 10**6, 1),
    "y_lon": round(lonz.y_lon * 1000, 1),
    "J_lon": round(lonz.J_lon * 10**12, 1),
    "lyambda_lon": round(lonz.lyambda_lon, 1),
    "sigma_kr": round(lonz.sigma_kr / 10**6, 1),
    "etta": round(lonz.etta, 3),
    "Mass": round(lonz.Mass, 1),
}

# создание словаря для записи переменных панельного отсека
variables_panel = {
    "Ne_pan": round(pan.Ne_pan / 1000, 2),
    "Nr_pan": round(pan.Nr_pan / 1000, 2),
    "sigma_pan": pan.sigma_pan / 10**6,  # ceil
    "n_r": pan.n_r,
    "N_reb": round(pan.N_reb / 1000, 2),
    "F_reb": round(pan.F_reb * 10**6, 2),
    "b_el": round(pan.b_el * 1000, 1),  # ceil
    "deltha": round(pan.deltha * 1000, 1),
    "H1": pan.H1 * 1000,
    "H2": pan.H2 * 1000,
    "F_TOCHN": round(pan.F_TOCHN * 10**6, 2),
    "sigma_d_pan": round(pan.sigma_d_pan / 10**6, 1),  # ceil
    "sigma_kr_mestn_pan": round(pan.sigma_kr_mestn_pan / 10**6, 1),
    "y_pan": round(pan.y_pan * 1000, 1),
    "J_pan": round(pan.J_pan * 10**12, 1),
    "sigma_kr_pan": round(pan.sigma_kr_pan / 10**6, 1),  # ceil
    "etta_pan": round(pan.etta_pan, 3),
    "mass_pan": round(pan.mass_pan, 1),
}

# создание словаря для записи переменных ферменного отсека
variables_ferma = {
    "Ne_f": round(frm.Ne_f / 1000, 2),
    "Nr_f": round(frm.Nr_f / 1000, 2),
    "sigma_f": frm.sigma_f / 10**6,  # ceil
    "n_ster": frm.n_ster,
    "n_y": frm.n_y,  # ceil
    "Ny_f": round(frm.Ny_f / 1000, 1),
    "Lc": round(frm.Lc * 1000, 1),  # ceil
    "Nc_f": round(frm.Nc_f / 1000, 1),
    "F_f": round(frm.F_f * 10**6, 1),  # ceil
    "delta_f": frm.delta_f * 1000,
    "Dc": frm.Dc * 1000,
    "F_tube": round(frm.F_tube * 10**6, 1),
    "sigma_d_f": round(frm.sigma_d_f / 10**6, 1),
    "sigma_kr_mestn_f": round(frm.sigma_kr_mestn_f / 10**6, 1),
    "sigma_ust": round(frm.sigma_ust / 10**6, 1),
    "etta_f": round(frm.etta_f, 3),
    "Mass_f": round(frm.Mass_f, 1),
}

# создание переменной шаблона .docx документа
doc = DocxTemplate("dz/konstruirovanie_korpusov_la_9sem/template_dz.docx")

# Добавление картинок

plot_all_strng()
plot_all_lonz()
plot_all_pan()
plot_all_frm()

img = Image.open("dz/konstruirovanie_korpusov_la_9sem/Ugolok.png")
newimg = img.resize((450, 300))
newimg.save("dz/konstruirovanie_korpusov_la_9sem/tmpUgolok.png")

img = Image.open("dz/konstruirovanie_korpusov_la_9sem/Longeron.png")
newimg = img.resize((450, 300))
newimg.save("dz/konstruirovanie_korpusov_la_9sem/tmpLongeron.png")

img = Image.open("dz/konstruirovanie_korpusov_la_9sem/Panel.png")
newimg = img.resize((450, 300))
newimg.save("dz/konstruirovanie_korpusov_la_9sem/tmpPanel.png")

img = Image.open("dz/konstruirovanie_korpusov_la_9sem/Truba.png")
newimg = img.resize((450, 300))
newimg.save("dz/konstruirovanie_korpusov_la_9sem/tmpTruba.png")

image_stringer = InlineImage(
    doc,
    "dz/konstruirovanie_korpusov_la_9sem/tmpUgolok.png",
)

image_lon = InlineImage(
    doc,
    "dz/konstruirovanie_korpusov_la_9sem/tmpLongeron.png",
)

image_pan = InlineImage(
    doc,
    "dz/konstruirovanie_korpusov_la_9sem/tmpPanel.png",
)

image_tube = InlineImage(
    doc,
    "dz/konstruirovanie_korpusov_la_9sem/tmpTruba.png",
)

images = {
    "image_stringer": image_stringer,
    "image_lon": image_lon,
    "image_pan": image_pan,
    "image_tube": image_tube,
}

context = (
    context_top
    | constants
    | variables_stringer
    | variables_lonzheron
    | variables_panel
    | variables_ferma
    | images
)

# рендер всех словарей в документе
doc.render(context)

# сохранение документа
doc.save("dz/konstruirovanie_korpusov_la_9sem/generated_dz.docx")
