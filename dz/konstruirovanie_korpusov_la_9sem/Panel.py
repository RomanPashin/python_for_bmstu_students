import math

N = 4000 * 1000  # Сжимающая сила в Н
M = 700 * 1000  # Крутящий момент в Н*м

E = 72000 * 10**6  # Модуль Юнга материала в Па
RO = 2700  # плотность материала, кг/м3
SIGMA_T = 280 * 10**6  # предел текучести, Па
SIGMA_V = 400 * 10**6  # предел прочности, Па
f = 1.1  # коэффициент безопасности

D = 3  # Диаметр отсека в м
L = 1.3  # Длина отсека в м

Ne_pan = math.ceil(N + 2 * M / (D / 2))  # Эквивалентная нагрузка, Н
Nr_pan = f * Ne_pan  # Расчётная нагрузка, Н

sigma_pan = 270 * 10**6  # Па, разрушающее напряжение
n_r = 120  # количество ребер
N_reb = Nr_pan / n_r  # нагрузка на одно ребро, Н
F_reb = N_reb / sigma_pan  # площадь ребра, м2
b_el = math.pi * D / n_r  # ширина силового элемента, м
deltha = F_reb / b_el  # толщина обшивки в м
H1 = 2.5 / 1000  # толщина ребра, м
H2 = 23.5 / 1000  # высота ребра, м
F_TOCHN = b_el * deltha + H1 * H2  # уточненная площадь ребра, м2
sigma_d_pan = N_reb / F_TOCHN  # действующее напряжение в ребре, Па                ВЫВОД
# проверка на устойчивость
sigma_kr_mestn_pan = 0.4 * math.pi**2 / (12 * (1 - 0.3**2)) * E * (H1 / H2) ** 2

y_pan = (b_el * deltha * deltha / 2 + H2 * H1 * H2 / 2) / (
    b_el * deltha + H1 * H2
)  # координата нейтральной линии, м                                               ВЫВОД
J_pan = (
    b_el * deltha**3 / 12
    + H1 * H2**3 / 12
    + b_el * deltha * (deltha / 2 - y_pan) ** 2
    + H2 * H1 * (H2 / 2 - y_pan) ** 2
)  # момент инерции ребора, м4

sigma_kr_pan = (
    0.3 * 2 * E / (D / 2 * deltha) * math.sqrt(J_pan * H1 / b_el)
)  # напряжение потери общей устойчивости, Па
etta_pan = sigma_kr_pan / sigma_d_pan  # коэффициент запаса                       ВЫВОД
F_PANEL = (
    F_TOCHN * n_r
)  # площадь панели                                                   ВЫВОД
mass_pan = L * RO * F_PANEL  # масса отсека, кг                             ВЫВОД
mass_pan = round(mass_pan, 3)
"""
print("F reb ", F_reb)
print("между ребрами", b_el)
print("уточненная плоащь", F_TOCHN)
print("sigma d ", sigma_d_pan)
print("sigma kr mestn b ", sigma_kr_mestn_pan)
print("y ", y_pan)
print("J ", J_pan)
print("sigma kr ", sigma_kr_pan)
print("zapas ", etta_pan)
print("F panel ", F_PANEL)
print("mass ", mass_pan)
"""
