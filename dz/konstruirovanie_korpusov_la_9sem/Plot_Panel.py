import matplotlib.pyplot as plt
import numpy as np

# Задаем параметры панели


b_el = 79  # ширина силового элемента, мм
deltha = 2.1  # толщина обшивки в мм
H1 = 2.5  # толщина ребра, мм
H2 = 23.5  # высота ребра, мм

Ynl = 3.84  # мм- расстояние от нижней кромки обшивки до нейтральной линии

# Размер картинки
LfigX = 7  # дюйм- Размер картинки по Х
LfigY = 5  # дюйм- Размер картинки по Y
Zaz = 10  # мм- зазор между обшивкой и краем картинки
LfigXmm = LfigX * 25.4  # мм- Размер картинки

# Размерные линии
ii = 4  # мм - расстояние от линии до размера
hh = 2  # мм - высота шрифта


z = (H2 + deltha) / 2

# Создаем массив точек для построения профиля
# Профиль панели
x1 = np.array(
    [
        -H1 / 2,
        -H1 / 2,
        -b_el / 2,
        -b_el / 2,
        b_el / 2,
        b_el / 2,
        H1 / 2,
        H1 / 2,
        -H1 / 2,
    ]
)
y1 = np.array([z, -z + deltha, -z + deltha, -z, -z, -z + deltha, -z + deltha, z, z])

# Уравнение нейтральной линии
x3 = np.array([-b_el / 2, b_el / 2])
y3 = np.array([-z + Ynl, -z + Ynl])


# Отрисовка размерных линий
# Размер H1
x4 = np.array([-H1 / 2, -H1 / 2])
y4 = np.array([z, z + ii])
x5 = np.array([H1 / 2, H1 / 2])
y5 = np.array([z, z + ii])
x6 = np.array([-H1 / 2 - ii / 2, H1 / 2 + 2 * ii])
y6 = np.array([z + ii, z + ii])

# Размер B
x7 = np.array([-b_el / 2, -b_el / 2])
y7 = np.array([-z, -z - 2 * ii])
x8 = np.array([b_el / 2, b_el / 2])
y8 = np.array([-z, -z - 2 * ii])
x9 = np.array([-b_el / 2 - ii / 2, b_el / 2 + ii / 2])
y9 = np.array([-z - 2 * ii, -z - 2 * ii])

# Размер H2
x10 = np.array([-5 * ii, -5 * ii])
y10 = np.array([-z + deltha, z + ii / 2])
x11 = np.array([-H1 / 2 / 2, -5 * ii])
y11 = np.array([z, z])

# Размер y_nl
x12 = np.array([b_el / 2 - ii, b_el / 2 - ii])
y12 = np.array([-z - hh, -z + Ynl + ii])
x13 = np.array([b_el / 2 - 4 * ii, b_el / 2 - 2 * ii])
y13 = np.array([-z + Ynl + ii, -z + Ynl + ii])
x14 = np.array([b_el / 2 - 2 * ii, b_el / 2 - ii])
y14 = np.array([-z + Ynl + ii, -z + Ynl])

# Размер deltha
x15 = np.array([-b_el / 2, -b_el / 2 - 2 * ii])
y15 = np.array([-z, -z])
x16 = np.array([-b_el / 2, -b_el / 2 - 2 * ii])
y16 = np.array([-z + deltha, -z + deltha])
x17 = np.array([-b_el / 2 - 1.5 * ii, -b_el / 2 - 1.5 * ii])
y17 = np.array([-z - ii / 2, -z + deltha + 2 * ii])


def plot_all_pan():
    plt.figure(figsize=(LfigX, LfigY))  # Создать картинку с размерами Lfig
    plt.axes(
        xlim=(-(b_el / 2 + Zaz), (b_el / 2 + Zaz)),
        ylim=(-(b_el / 2 + Zaz) * LfigY / LfigX, ((b_el / 2 + Zaz) * LfigY / LfigX)),
    )  # Масштабы картинки по осям
    plt.plot(x1, y1, "k", lw=2)  # Отрисовка первого графика. Толщина 1.5
    # Отрисовка нейтральной линии. -. обозначает штрих пунктир
    plt.plot(x3, y3, "k-.", lw=1)

    # Размерные линии
    # Размер H1
    plt.plot(x4, y4, "k", lw=1)
    plt.plot(x5, y5, "k", lw=1)
    plt.plot(x6, y6, "k", lw=1)
    # Размер B_el
    plt.plot(x7, y7, "k", lw=1)
    plt.plot(x8, y8, "k", lw=1)
    plt.plot(x9, y9, "k", lw=1)
    # Размер H2
    plt.plot(x10, y10, "k", lw=1)
    plt.plot(x11, y11, "k", lw=1)
    # Размер y_nl
    plt.plot(x12, y12, "k", lw=1)
    plt.plot(x13, y13, "k", lw=1)
    plt.plot(x14, y14, "k", lw=1)

    # Размер deltha
    plt.plot(x15, y15, "k", lw=1)
    plt.plot(x16, y16, "k", lw=1)
    plt.plot(x17, y17, "k", lw=1)

    # Подпись размеров
    plt.text(H1 / 2, z + ii + 0.5, H1)  # Размер H1
    plt.text(-ii / 2, -z - 2 * ii + hh, b_el)  # Размер B_el
    plt.text(-7 * ii, 0, H2)  # Размер H2
    plt.text(b_el / 2 - 4 * ii, -z + Ynl + ii + 0.5, Ynl)  # Размер Ynl
    plt.text(-b_el / 2 - 3 * ii, -z + deltha + ii, deltha)  # Размер deltha

    plt.fill_between(x1, y1, facecolor="none", hatch="/", linewidth=0.0)
    plt.axis("off")  # Отключить отображение осей
    plt.show()  # Отрисовка картинки
    plt.savefig("dz/konstruirovanie_korpusov_la_9sem/Panel.png")
