import matplotlib.pyplot as plt
import numpy as np

# Задаем параметры профиля
h = 3  # мм- Толщина лонжерона
B = 54  # мм- Высота лонжерона
L1 = 70  # мм- Длина полки лонжерона
L2 = L1 + 2 * 12  # мм- Максимальная длина лонжерона
Delt = 1.2  # мм- Толщина присоединенной обшивки
Lpr = 44  # мм - Длина присоединенной обшивки
L = Lpr + L1 + 12  # мм- Длина обшивки
Ynl = 20  # мм- расстояние от нижней кромки обшивки до нейтральной линии

# Размер картинки
LfigX = 7  # дюйм- Размер картинки по Х
LfigY = 5  # дюйм- Размер картинки по Y
Zaz = 10  # мм- зазор между обшивкой и краем картинки
LfigXmm = LfigX * 25.4  # мм- Размер картинки

# Размерные линии
ii = 4  # мм - расстояние от линии до размера
hh = 2  # мм - высота шрифта

z = (B + Delt) / 2

# Создаем массив точек для построения профиля
# Профиль уголка
x1 = np.array(
    [
        -L1 / 2,
        -L1 / 2,
        -L2 / 2,
        -L2 / 2,
        -L1 / 2 + h,
        -L1 / 2 + h,
        L1 / 2 - h,
        L1 / 2 - h,
        L2 / 2,
        L2 / 2,
        L1 / 2,
        L1 / 2,
        -L1 / 2,
    ]
)
y1 = np.array(
    [
        z,
        -z + Delt + h,
        -z + Delt + h,
        -z + Delt,
        -z + Delt,
        z - h,
        z - h,
        -z + Delt,
        -z + Delt,
        -z + Delt + h,
        -z + Delt + h,
        z,
        z,
    ]
)
# Профиль обшивки
x2 = np.array([-L / 2, -L / 2, L / 2, L / 2, -L / 2])
y2 = np.array([-z + Delt, -z, -z, -z + Delt, -z + Delt])
# Уравнение нейтральной линии
x3 = np.array([-L / 2, L / 2])
y3 = np.array([-z + Ynl, -z + Ynl])
# Отрисовка размерных  линий
# Размер L1
x4 = np.array([-L1 / 2, -L1 / 2])
y4 = np.array([z, z + ii])
x5 = np.array([L1 / 2, L1 / 2])
y5 = np.array([z, z + ii])
x6 = np.array([-L1 / 2, L1 / 2])
y6 = np.array([z + ii, z + ii])
# Размер L2
x7 = np.array([-L2 / 2, -L2 / 2])
y7 = np.array([-z + Delt + h, z + 2 * ii])
x8 = np.array([L2 / 2, L2 / 2])
y8 = np.array([-z + Delt + h, z + 2 * ii])
x9 = np.array([-L2 / 2, L2 / 2])
y9 = np.array([z + 2 * ii, z + 2 * ii])
# Размер Lpr левый
x10 = np.array([-L / 2, -L / 2])
y10 = np.array([-z, -z - 2 * ii - h])
x11 = np.array([-L / 2 + Lpr, -L / 2 + Lpr])
y11 = np.array([-z, -z - 2 * ii - h])
x12 = np.array([-L / 2, -L / 2 + Lpr])
y12 = np.array([-z - 2 * ii - h, -z - 2 * ii - h])
# Размер Lpr правый
x13 = np.array([L / 2, L / 2])
y13 = np.array([-z, -z - 2 * ii - h])
x14 = np.array([L / 2 - Lpr, L / 2 - Lpr])
y14 = np.array([-z, -z - 2 * ii - h])
x15 = np.array([L / 2, L / 2 - Lpr])
y15 = np.array([-z - 2 * ii - h, -z - 2 * ii - h])
# Размер L
x16 = np.array([L / 2, L / 2])
y16 = np.array([-z, -z - 3 * ii - h])
x17 = np.array([-L / 2, -L / 2])
y17 = np.array([-z, -z - 3 * ii - h])
x18 = np.array([L / 2, -L / 2])
y18 = np.array([-z - 3 * ii - h, -z - 3 * ii - h])
# Размер Delta
x19 = np.array([L / 2, L / 2 + 2 * ii])
y19 = np.array([-z, -z])
x20 = np.array([L / 2, L / 2 + 2 * ii])
y20 = np.array([-z + Delt, -z + Delt])
x21 = np.array([L / 2 + 2 * ii, L / 2 + 2 * ii])
y21 = np.array([-z + Delt, -z - ii - hh])
# Размер h
x22 = np.array([L / 2, L / 2 + 2 * ii])
y22 = np.array([-z + Delt, -z + Delt])
x23 = np.array([L2 / 2, L / 2 + 2 * ii])
y23 = np.array([-z + Delt + h, -z + Delt + h])
x24 = np.array([L / 2 + 2 * ii, L / 2 + 2 * ii])
y24 = np.array([-z + Delt, -z + h + ii + hh])
# Размер Nl
x25 = np.array([-L / 2, -L / 2 - ii])
y25 = np.array([-z, -z])
x26 = np.array([-L / 2, -L / 2 - ii])
y26 = np.array([-z + Ynl, -z + Ynl])
x27 = np.array([-L / 2 - ii, -L / 2 - ii])
y27 = np.array([-z, -z + Ynl])
# Левая осевая
x28 = np.array([(-L1 - 12) / 2, (-L1 - 12) / 2])
y28 = np.array([-z + 2 * ii, -z - 1.5 * ii])
# Правая осевая
x29 = np.array([(L1 + 12) / 2, (L1 + 12) / 2])
y29 = np.array([-z + 2 * ii, -z - 1.5 * ii])


def plot_all_lonz():
    plt.figure(figsize=(LfigX, LfigY))  # Создать картинку с размерами Lfig
    plt.axes(
        xlim=(-(L / 2 + Zaz), (L / 2 + Zaz)),
        ylim=(-(L / 2 + Zaz) * LfigY / LfigX, ((L / 2 + Zaz) * LfigY / LfigX)),
    )  # Масштабы картинки по осям
    plt.plot(x1, y1, "k", lw=1.5)  # Отрисовка первого графика. Толщина 1.5
    plt.plot(x2, y2, "k", lw=1.5)  # Отрисовка Второго графика
    # Отрисовка нейтральной линии. -. обозначает штрих пунктир
    plt.plot(x3, y3, "k-.", lw=1)
    # Размерные линии
    # Размер L1
    plt.plot(x4, y4, "k", lw=1)
    plt.plot(x5, y5, "k", lw=1)
    plt.plot(x6, y6, "k", lw=1)
    # Размер L2
    plt.plot(x7, y7, "k", lw=1)
    plt.plot(x8, y8, "k", lw=1)
    plt.plot(x9, y9, "k", lw=1)
    # Размер Lpr левый
    plt.plot(x10, y10, "k", lw=1)
    plt.plot(x11, y11, "k", lw=1)
    plt.plot(x12, y12, "k", lw=1)
    # Размер Lpr правый
    plt.plot(x13, y13, "k", lw=1)
    plt.plot(x14, y14, "k", lw=1)
    plt.plot(x15, y15, "k", lw=1)
    # Размер L
    plt.plot(x16, y16, "k", lw=1)
    plt.plot(x17, y17, "k", lw=1)
    plt.plot(x18, y18, "k", lw=1)
    # Размер Delta
    plt.plot(x19, y19, "k", lw=1)
    plt.plot(x20, y20, "k", lw=1)
    plt.plot(x21, y21, "k", lw=1)
    # Размер h
    plt.plot(x22, y22, "k", lw=1)
    plt.plot(x23, y23, "k", lw=1)
    plt.plot(x24, y24, "k", lw=1)
    # Размер Nl
    plt.plot(x25, y25, "k", lw=1)
    plt.plot(x26, y26, "k", lw=1)
    plt.plot(x27, y27, "k", lw=1)
    plt.plot(x28, y28, "k-.", lw=1)  # Левая осевая
    plt.plot(x29, y29, "k-.", lw=1)  # Правая осевая

    # Подпись размеров
    plt.text(0, z + ii + 0.5, L1)  # Размер L1
    plt.text(0, z + 2 * ii + 0.5, L2)  # Размер L2
    plt.text((-L + Lpr) / 2, -z - 2 * ii - h + 0.5, Lpr)  # Размер Lpr левый
    plt.text((L - Lpr) / 2, -z - 2 * ii - h + 0.5, Lpr)  # Размер Lpr правый
    plt.text(0, -z - 3 * ii - h + 0.5, L)  # Размер L
    plt.text(L / 2 + ii - hh, -z - ii - 0.5, Delt)  # Размер Delt
    plt.text(L / 2 + ii - hh, -z + h + ii + 0.5, h)  # Размер h
    plt.text(-L / 2 - 2 * ii - hh, -z + Ynl / 2, Ynl)  # Размер Ynl

    # Штриховка
    plt.fill_between(x1, y1, facecolor="none", hatch="/", linewidth=0.0)
    plt.fill_between(x2, y2, facecolor="none", hatch="X", linewidth=0.0)

    plt.axis("off")  # Отключить отображение осей
    plt.show()  # Отрисовка картинки
    plt.savefig("dz/konstruirovanie_korpusov_la_9sem/Longeron.png")
