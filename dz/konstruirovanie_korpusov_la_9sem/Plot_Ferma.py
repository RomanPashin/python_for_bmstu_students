import matplotlib.pyplot as plt
import numpy as np

# Задаем параметры профиля
r1 = 31.5  # Внешний радиус
r2 = 29  # Внутренний радиус

# Размер картинки
LfigX = 7  # дюйм- Размер картинки по Х
LfigY = 5  # дюйм- Размер картинки по Y
Zaz = 10  # мм- зазор между обшивкой и краем картинки
LfigXmm = LfigX * 25.4  # мм- Размер картинки

# Размерные линии
ii = 4  # мм - расстояние от линии до размера
hh = 2  # мм - высота шрифта

# Профиль трубы и его
t = np.arange(0, 2 * np.pi, 0.01)


def plot_all_frm():
    plt.figure(figsize=(LfigX, LfigY))  # Создать картинку с размерами Lfig
    plt.axes(
        xlim=(-(r1 + Zaz), (r1 + Zaz)),
        ylim=(-(r1 + Zaz) * LfigY / LfigX, ((r1 + Zaz) * LfigY / LfigX)),
    )  # Масштабы картинки по осям
    plt.plot(
        r1 * np.sin(t),
        r1 * np.cos(t),
        "k",
        lw=1.5,
    )
    plt.plot(
        r2 * np.sin(t),
        r2 * np.cos(t),
        "k",
        lw=1.5,
    )
    plt.axis("equal")
    # Вертикальная ось
    x1 = np.array([-r1 - 2, r1 + 2])
    y1 = np.array([-0.2, 0.2])
    # Горизонтальная ось
    x2 = np.array([-0.2, 0.2])
    y2 = np.array([-r1 - 2, r1 + 2])
    # Размерные линии
    # Размер D1
    x3 = np.array([r1 * np.sin(5 / 4 * np.pi), r1, r1 + ii + hh])
    y3 = np.array([r1 * np.cos(5 / 4 * np.pi), r1, r1])
    # Размер Delta
    x4 = np.array([r2 * np.cos(3 / 4 * np.pi), -3 / 4 * r1, -3 / 4 * r1 - ii - hh])
    y4 = np.array([r2 * np.sin(3 / 4 * np.pi), 3 / 4 * r1, 3 / 4 * r1])
    # Отрисовка осевые
    plt.plot(x1, y1, "k-.", lw=1)
    plt.plot(x2, y2, "k-.", lw=1)
    # Отрисовка размеров
    plt.plot(x3, y3, "k", lw=1)
    plt.plot(x4, y4, "k", lw=1)

    # Штриховка
    plt.fill_between(
        r1 * np.sin(t), r1 * np.cos(t), facecolor="none", hatch="/", linewidth=0.0
    )
    plt.fill_between(r2 * np.sin(t), r2 * np.cos(t), color="white")

    # Подпись размеров
    plt.text(r1, r1 + 0.5, 2 * r1)  # Размер D1
    plt.text(-3 / 4 * r1 - ii - hh, 3 / 4 * r1 + 0.5, r1 - r2)  # Размер Delta

    plt.axis("off")  # Отключить отображение осей
    plt.show()  # Отрисовка картинки
    plt.savefig("dz/konstruirovanie_korpusov_la_9sem/Truba.png")
