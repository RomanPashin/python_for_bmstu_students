import numpy as np

"""
Файл задает параметры энерголовушки.

ВНОСИМЫЕ КОНСТАНТЫ
_________________

- catch_mass - масса подвешенного грузика, модулирующего энерголовушку, [кг]
- length_unloaded - невозмущенная длина проволоки, [м]
- length_loaded - длина проволоки в подвешенном состоянии груза, [м]
- elastic_modulus - модуль упругости материала проволоки, [Па]
- diameter_wire - диаметр проволоки, [мм]
- section_area_wire - площадь проволоки, [м^2]

ПРОГРАММНО РАССЧИТЫВАЕМЫЕ КОНСТАНТЫ
___________________________________
- section_area_wire - площадь сечения проволоки, [м^2]
- length_delta - изменение длины пружины, [м]
- force_tension_start - начальная сила натяжения пружины, [H]

"""
catch_mass = 0.03
length_unloaded = 0.1
length_loaded = 0.11
elastic_modulus = 7 * 10**10
diameter_wire = 1
force_external = 500

section_area_wire = (diameter_wire / 1000) ** 2 * np.pi / 4
length_delta = length_loaded - length_unloaded
force_tension_start = (
    elastic_modulus * section_area_wire * (length_delta / length_unloaded)
)
