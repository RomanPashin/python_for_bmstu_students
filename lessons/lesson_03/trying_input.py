# %%
# Строки, input
greetings = "Привет, "
name = "Ivan Ivanov"
endings = "!"

# %%
# Строки можно складывать, т.е сшивать
print(greetings + name + endings)

# %%
print("Пожалуйста, введите Ваше имя:")
name = input()
print(greetings + name + endings)

# %%
name = input("Введите Ваше имя ещё раз:\n")
print(greetings + name + endings)
