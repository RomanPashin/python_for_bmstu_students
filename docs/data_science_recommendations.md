# Что ещё посмотреть для работы в Data Science

## С чего начать

Поискать и посмотреть обзорные лекции, чтобы знать в каком направлении двигаться. Тема
Data Science обширная, можно заниматься компьютерным зрением, лингвистическими задачами,
обработкой сигналов и звука, обработкой табличных данных и т.д.

## Университетские или корпоративные курсы

Курсы должны быть бесплатные, на них должно быть тяжело поступить. Например, ШАД (школа
анализа данных от Яндекса), не путать с Яндекс-Практикумом, это такая же платная помойка
как GeekBrains, SkillBox, SkillFactory и т.д. Можно рассмотреть бесплатную аспирантуру в
РЭШ или ВШЭ.

## Книги

- [Dive into Deep Learning](https://d2l.ai/) - обновляемая open source книга, принятая
  во многих ВУЗах по всему миру. Можно выбрать вариант книги для одного из трёх наиболее
  популярных DL фреймворков для Python: PyTorch, Tensorflow, MXNet.
- [Pro Git](https://git-scm.com/book/ru/v2) - можно почитать первые 25-30% для начала
  уверенной работы с Git.
- [Python 3 Object-Oriented Programming Third
Edition](https://chadshare.com/Books/IT/Security/python3objectorientedprogramming.pdf) -
тут есть про паттерны проектирования на Python (главы 10, 11).

## Курсы в интернете

### Stepik

- [Нейронные сети и компьютерное зрение](https://stepik.org/course/50352/syllabus),
  [Нейронные сети и обработка текста](https://stepik.org/course/54098/syllabus) от
  Samsung
- [Практикум по математике и Python](https://stepik.org/course/3356/syllabus)
- [Основы статистики](https://stepik.org/course/76), есть ещё часть
  [2](https://stepik.org/course/524/syllabus) и
  [3](https://stepik.org/course/2152/syllabus).
- [Deep Learning](https://stepik.org/course/160792/promo) от МФТИ, сам не проходил,
  советовали.
- ["Поколение python" для продвинутых](https://stepik.org/course/68343/syllabus) - не
  проходил, [Инди-курс программирования на
  Python](https://stepik.org/course/63085/syllabus) - просмотрел при подготовке курса.

### Karpov courses

- [Симулятор SQL](https://karpov.courses/simulator-sql), не проверял, рекомендовали.
- [Docker](https://karpov.courses/docker), не проверял, рекомендовали.

### ODS

- [Открытый курс машинного обучения. Тема 1. Первичный анализ данных с
  Pandas](https://habr.com/ru/companies/ods/articles/322626/), не проверял,
  рекомендовали.

### Yandex handbook

- [Алгоритмы](https://academy.yandex.ru/handbook/algorithms)
- [Учебник по машинному обучению](https://education.yandex.ru/handbook/ml)
- [Основы Python](https://education.yandex.ru/handbook/python)

## Telegram каналы

Их [много](https://t.me/addlist/Yq5b7gQjzGs5NjIy), но читать их перестал, т.к. слишком
много разрозненной информации. Почитываю только [Data
Secrets](https://t.me/data_secrets).

## Youtube каналы

- [Two Minute Paper](https://www.youtube.com/@TwoMinutePapers) - короткие
  развлекательные видео о самых передовых достижениях в ИИ с забавным индийским
  акцентом
- [Диджитализируй](https://www.youtube.com/@t0digital) - в основном про Python, про
  фишки новых версий, про сложные вещи простым языком
- [3Blue1Brown](https://www.youtube.com/@3blue1brown) - весьма наглядно с качественными
  анимациями о довольно сложных понятиях в математике. Анимации сделаны в Python, автор
  канала опубликовал свою библиотеку, можно делать такие же красивые видео.
- [StatQuest](https://www.youtube.com/@statquest) - наглядное объяснение понятий в
  статистике

## Необходимые предметные области

- Математика вообще и в частности статистика
- Python
- Алгоритмы и структуры данных
- Pandas, numpy
- SQL, базы данных
- Linux, Docker
- Git

## Прочее

### Проекты

- [kaggle.com](kaggle.com), не зарегистрирован.
- Участие в хакатонах, не участвовал.
- pet-projects - любые задачи для наработки практики решения задач за пределами
  тренажеров. Можно пытаться совмещать интерес и практическую пользу, например, внедрить
  машинное обучение/автоматизацию в то, чем уже занимаетесь.

### Игры

- [Oh my git](https://ohmygit.org/), не играл.

### Задачи

- [Leet code.
  Python](https://play.google.com/store/apps/details?id=com.machinelearningforsmallbusiness.leetcodepython&pcampaignid=web_share) - приложение с задачами разного уровня сложности.
- [Code forces](https://codeforces.com/), не регистрировался, рекомендовали как аналог
  Leet Code.

<!--
TODO
1. Сгруппировать по разделам, а не по площадкам
2. Добавить ссылку на бесплатный интерактивный тренажер яндекс-практикума, не проходил,
рекомендовали.
https://practicum.yandex.ru/sql-database-basics/
-->
