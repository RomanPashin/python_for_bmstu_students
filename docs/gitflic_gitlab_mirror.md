# Работа с репозиториями

## 1. Переход с Gitlab на Gitflic

### 1.1. Исправить раздел в файле .git/config

При использовании Gitlab в файле .git/config раздел \[remote "origin"\] выглядит
следующим образом:

```config
[remote "origin"]
	url = git@gitlab.com:Zaynulla/python_for_bmstu_students.git
	fetch = +refs/heads/*:refs/remotes/origin/*
```

Раздел в этом файле необходимо скорректировать следующим образом:

```config
[remote "origin"]
	url = git@gitflic.ru:zhumaev/python_for_bmstu_students.git
	fetch = +refs/heads/*:refs/remotes/origin/*
```

### 1.2. Добавить ssh ключ в Gitflic

В веб-интерфейсе Gitflic аналогично тому как делали для Gitlab, см. инструкцию на
[Stepik](https://stepik.org/lesson/1099950/step/1?unit=1110938).

## 2. Два репозитория в режиме зеркала

Вместо перехода на gitflic можно добавить этот репозиторий в качестве зеркала.
Необходимо открыть файл .git/config и отредактировать раздел \[remote "origin"\]
следующим образом:

```config
[remote "origin"]
	url = git@gitlab.com:Zaynulla/python_for_bmstu_students.git
	url = git@gitflic.ru:zhumaev/python_for_bmstu_students.git
	fetch = +refs/heads/*:refs/remotes/origin/*
	pushurl = git@gitlab.com:Zaynulla/python_for_bmstu_students.git
	pushurl = git@gitflic.ru:zhumaev/python_for_bmstu_students.git
```
